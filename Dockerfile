FROM alpine:3.17.3

WORKDIR /tmp

RUN wget https://github.com/zegl/kube-score/releases/download/v1.16.1/kube-score_1.16.1_linux_amd64.tar.gz && tar xf kube-score_1.16.1_linux_amd64.tar.gz kube-score && mv kube-score /usr/local/bin/kube-score && rm -f kube-score_1.16.1_linux_amd64.tar.gz

CMD ["kube-score", "--help"]
